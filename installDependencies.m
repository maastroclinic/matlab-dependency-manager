function installDependencies(root)
    if nargin == 0
        disp('please provide the root folder of this repository so the install can run correctly!')
        return;
    end
    
    if ~ischar(root) || ~isdir(root)
        disp('please provide a valid root folder as string!')
    end
    
    home = cd;
    cd(root);
    if ~isdir(fullfile(cd,'setup'))
        disp(['no setup folder found for ' root ', no dependencies to install!'])
        cd(home);
        return;
    end
    
    addpath(fullfile(cd,'setup'));
    
    dependencies = setup();
    
    for dependency = dependencies
        if isdir(['./src/externalLib/' dependency.name])
            disp([dependency.name ' is already installed, please run updateDependencies.m if you want to update'])
            continue;
        end
        
        clone(dependency);
        
        if ~isempty(dependency.tag)
            checkoutTag(dependency);
            continue;
        end
        
        if ~isempty(dependency.branch)
            checkoutBranch(dependency);
        end
        
        disp(['searching dependency for other dependencies: ' dependency.folder])
        installDependencies(dependency.folder);
    end
    rmpath(fullfile(cd,'setup'));
    cd(home)
end