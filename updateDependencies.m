function updateDependencies(root)
    if nargin == 0
        disp('please provide the root folder of this repository so the update can run correctly!')
        return;
    end
    
    if ~ischar(root) || ~isdir(root)
        disp('please provide a valid root folder as string!')
    end
    
    home = cd;
    cd(root);
    if ~isdir(fullfile(cd,'setup'))
        disp(['no setup folder found for ' root ', no dependencies to update!'])
        cd(home);
        return;
    end
    
    addpath(fullfile(cd,'setup'));
    
    dependencies = setup();
    
    for dependency = dependencies
        fetch(dependency);
        
        if ~isempty(dependency.tag)
            checkoutTag(dependency);
        elseif ~isempty(dependency.branch)
            checkoutBranch(dependency);
        else
            checkout(dependency);
        end
        
        disp(['searching dependency for other dependencies: ' dependency.folder])
        updateDependencies(dependency.folder)
    end
    rmpath(fullfile(cd,'setup'));
    cd(home);
end