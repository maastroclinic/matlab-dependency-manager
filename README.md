# README #

This library was written for Matlab 2015b and was tested for this version Newer versions should be verified by the user.

### What is this repository for? ###

* This dependency manager uses a Matlab Git wrapper to install and update dependencies using a standard Matlab repository format.
* Version: 1.0

### How do I get set up? ###

* [Install Git;](https://git-scm.com/downloads "Install Git")
* Download or clone this repository;
* Add the repository to your permanent Matlab path;

### How to use? ###

This dependency manager works if the Matlab repository is ordered using certain file structure:

* repository_root
	* ./src	: folder to store source code
	* ./test: folder to store any test code
	* ./resources: folder to store any (binary) resources required by ./src or ./test
	* ./setup: folder with setup functions

### Setting up the dependencies ###

the setup folder has to contain a setup.m. This function should return a list of DependencyContainer objects that specify dependency information. The minimum requirement is the repository url. A branch or tag can be specified to further configure a dependency.


### setup.m example ###

    function dependencies = setup()
        dependencies(1) = DependencyContainer('http://fs-bitbucket-01/scm/diu/dicom-file-interface.git');
        dependencies(1).branch = 'feature/DIU-119-add-dependency-management';
        dependencies(2) = DependencyContainer('https://github.com/TimLustberg/jsonlab');
        dependencies(2).tag = '1.0';
        dependencies(3) = DependencyContainer('https://github.com/TimLustberg/logging4matlab');
    end

### Who do I talk to? ###

* MAASTRO Clinic Software Development
* Tim Lustberg 
	* tim.lustberg@maastro.nl