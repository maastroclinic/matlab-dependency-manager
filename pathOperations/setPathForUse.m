function setPathForUse(root)

    warning off; %#ok<WNOFF>

    if nargin == 0
        disp('please provide the root folder of this repository so the setPath can run correctly!')
        return;
    end

    if ~ischar(root) || ~isdir(root)
        disp('please provide a valid root folder as string!')
    end

    home = cd;
    cd(root);
    
    addpath(genpath(cd));
    rmpath(genpath(fullfile(cd, '.git')));
    rmpath(genpath(fullfile(cd, 'setup')));
    rmpath(genpath(fullfile(cd, 'test')));
    rmpath(genpath(fullfile(cd, 'resources')));
    rmpath(genpath(fullfile(cd, 'src', 'externalLib')));
    
    if ~isdir(fullfile(cd,'setup'))
        disp(['no setup folder found for ' root ', no dependencies to update!'])
        cd(home);
        return;
    end
    
    rmpath(genpath(fullfile(cd, 'src', 'externalLib')));

    addpath(fullfile(cd,'setup'));
    dependencies = setup();
    for dependency = dependencies
        disp(['searching dependency for other dependencies: ' dependency.folder])
        setPathForTesting(dependency.folder)
    end
    rmpath(fullfile(cd,'setup'));
    cd(home);
    
    warning on; %#ok<WNON>
end