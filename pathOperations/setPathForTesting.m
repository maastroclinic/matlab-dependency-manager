function setPathForTesting(root)

    warning off; %#ok<WNOFF>

    if nargin == 0
        disp('please provide the root folder of this repository so the setPath can run correctly!')
        return;
    end

    if ~ischar(root) || ~isdir(root)
        disp('please provide a valid root folder as string!')
    end
    
    setPathForUse(root);

    home = cd;
    cd(root);
    
    addpath(genpath(fullfile(cd, 'test')));
    cd(home);
    warning on; %#ok<WNON>
end