classdef DependencyContainer
    %leave tag and branch empty to work on latest commit of the master
    %tags are prioritized over branches if both are provided
    properties
        url
        name
        tag
        branch
        folder
    end
    
    methods
        function this = DependencyContainer(url)
            if nargin == 0
                return;
            end
            
            this.url = url;
        end
        
        function out = get.name(this)
            slashedIndex = regexp(this.url, '/');
            beginIndex = slashedIndex(end) + 1;
            endIndex = regexp(this.url, '\.git') - 1;
            if isempty(endIndex)
                out = this.url(beginIndex:end);
            else
                out = this.url(beginIndex:endIndex);
            end
        end
        
        function out = get.folder(this)
            out = ['./src/externalLib/' this.name];
        end
    end
end